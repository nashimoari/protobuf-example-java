package com.gitlab.nashimoari.protobuf;

import example.simple.Simple.SimpleMessage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class SimpleMain {
    public static void main(String[] args) throws IOException {
        System.out.println("Hello world!");

        SimpleMessage.Builder builder = SimpleMessage.newBuilder();

//        builder.setId(42);
//        builder.setIsSimple(true);
//        builder.setName("My Simple Message Name");

        // simple fields
        builder.setId(42) // set the id filed
                .setIsSimple(true) // set the is_simple filed
                .setName("My Simple Message Name"); // set the name filed

        builder.addSampleList(1)
                .addSampleList(2)
                .addSampleList(3);

        builder.addAllSampleList(Arrays.asList(4, 5, 6));

        // builder.setSampleList(3, 42);

        System.out.println(builder.toString());

        SimpleMessage message = builder.build();

        // now we can read data from message
        /* message.getId();
         message.getName();
         */

        // write the protocol buffers binary to a file
        FileOutputStream outputStream = new FileOutputStream("simple_message.bin");
        message.writeTo(outputStream);
        outputStream.close();


        // send as byte array
        // byte[] bytes = message.toByteArray();

        // read the protocol buffers binary from a file
        System.out.println("Reading from file...");
        FileInputStream fileInputStream = new FileInputStream("simple_message.bin");
        SimpleMessage messageFromFile = SimpleMessage.parseFrom(fileInputStream);
        System.out.println(messageFromFile);
    }
}
